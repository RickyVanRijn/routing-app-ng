# RoutingApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.4.

To run succesfully on Windows 10 you will need to run `npm install -g npm@6`.
There are some known issues with npm 7.x.x and the @angular/cli. (https://github.com/angular/angular-cli/issues/19957)

Also when using PowerShell in Windows 10 you will there is a slight bump to overcome.
Executing scripts from Windows untrusted sources is disabled, to enable it take the following steps:
- Run powershell as Administrator
- Run the command: `Set-ExecutionPolicy RemoteSigned` and reply Yes
- Go to the projects folder to try `ng new my-app-name` again, it should work.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Debugging

For VS Code there is an extension which is free to install. This extension allows easy debugging with Google Chrome. You can find it here: (https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome).

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
