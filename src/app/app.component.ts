import { Component } from '@angular/core';

//Metadata for class
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

//class implementation
export class AppComponent {
  title = 'New App';
}
